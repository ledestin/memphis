require_relative "kata"

describe "Jobs.order" do
  it "returns [] if input is empty" do
    expect(Jobs.order "").to eq []
  end

  it "handles a single job input" do
    expect(Jobs.order "a =>").to eq ["a"]
  end

  it "handles 3 jobs input without dependencies" do
    ordered_jobs_in_no_particular_order = Jobs.order("a =>\nb =>\nc =>")
    expect(ordered_jobs_in_no_particular_order.sort).to eq ["a", "b", "c"]
  end

  it "handles 3 jobs with 1 dependency" do
    input_jobs = <<~EOF
      a =>
      b => c
      c
    EOF

    expect(Jobs.order input_jobs).to eq ["a", "c", "b"]
  end

  it "handles multiple dependencies" do
    input_jobs = <<~EOF
      a =>
      b => c
      c => f
      d => a
      e => b
      f =>
    EOF

    expect(Jobs.order input_jobs).to eq ["a", "f", "c", "b", "d", "e"]
  end

  it "detects a circular dependency" do
    input_jobs = <<~EOF
      a =>
      b =>
      c => c
    EOF

    expect { Jobs.order input_jobs }.to \
      raise_error("Job `c' can't depend on itself")
  end

  it "detects a complex circular dependency" do
    input_jobs = <<~EOF
      a =>
      b => c
      c => f
      d => a
      e =>
      f => b
    EOF

    expect { Jobs.order input_jobs }.to \
      raise_error("Jobs can't have circular dependencies")
  end
end
