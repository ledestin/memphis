require_relative "tsortable_hash"

module Jobs
  Job = Struct.new(:name, :dependency) do
    def depends_on_itself?
      name == dependency
    end

    def to_s
      name
    end
  end

  def self.order(jobs)
    return [] if jobs.empty?

    parse(jobs).inject(TSortableHash.new) do |sort_engine, new_job|
      check_dependency_on_itself new_job

      sort_engine[new_job.name] = Array(new_job.dependency)
      sort_engine
    end.tsort
  rescue TSort::Cyclic
    raise "Jobs can't have circular dependencies"
  end

  def self.parse(jobs)
    jobs.split("\n").map do |job_definition|
      Job.new *job_definition.split(/\s*=>\s*/)
    end
  end

  def self.check_dependency_on_itself(job)
    raise "Job `#{job}' can't depend on itself" \
      if job.depends_on_itself?
  end
end
